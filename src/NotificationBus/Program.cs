﻿using Coscine.ApiCommons;
using Coscine.Configuration;

namespace Coscine.Api.NotificationBus
{
    public class Program : AbstractProgram<ConsulConfiguration>
    {
        public static void Main()
        {
            InitializeInternalWebService<Startup>();
        }
    }
}
