﻿using Coscine.Database.DataModel;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Coscine.Api.NotificationBus;

public class NotificationParameterObject
{
    public List<User> Users { get; set; }

    public string Action { get; set; }

    public string ProjectId { get; set; }

    public JObject Args { get; set; }

    public string Href { get; set; }

    public NotificationParameterObject(string action, JArray users, string projectId, JObject args, string href)
    {
        Action = action;
        Users = GenerateUsers(users);
        ProjectId = projectId;
        Args = args;
        Href = href;
    }

    private static List<User> GenerateUsers(JArray emails)
    {
        // A necessary workaround to make sending emails possible, when the recepient is not a Coscine user (e.g. servicedesk)
        var users = new List<User>();
        foreach (var email in emails)
        {
            User user;
            try
            {
                user = email.ToObject<User>(); 
                users.Add(user);
            }
            catch
            {
                var emailString = email.ToObject<string>();
                users.Add(new User
                {
                    EmailAddress = emailString,
                });
            }
        }
        return users;
    }
}
